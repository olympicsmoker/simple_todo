module TasksHelper
  def date_range(start, finish)
    "#{format_datetime(start)} - #{format_datetime(finish)}"
  end

  def format_datetime(date)
    date.strftime('%d.%m.%Y %H:%M')
  end
end
