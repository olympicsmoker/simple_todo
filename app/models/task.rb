class Task < ActiveRecord::Base
  validates :name, :starts_at, :ends_at, presence: true
  validates :starts_at, :ends_at, overlap: true
  paginates_per 10

  belongs_to :users

  scope :all_users_tasks, ->(id) { where(user_id: id).order(starts_at: :asc)}

  def started?
    starts_at <= DateTime.now
  end
end
