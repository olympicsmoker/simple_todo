class TasksController < ApplicationController
  expose(:task, attributes: :task_params)
  expose(:tasks) {Task.all_users_tasks(current_user.id).page(params[:page])}

  def create
    if task.save 
      redirect_to tasks_path, :notice => "Successfully created article."
    else
      render :new
    end
  end

  def update
    if task.save
      redirect_to(tasks_path)
    else
      render :edit
    end
  end

   def destroy
    task.destroy
    redirect_to tasks_path, :notice => "Successfully destroyed article."
  end

  private

  def task_params
    params.require(:task).permit(:name, :starts_at, :ends_at)
  end

end
