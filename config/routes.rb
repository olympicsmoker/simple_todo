Rails.application.routes.draw do
  devise_for :users
  resources :tasks
  root to: 'visitors#index'
end
